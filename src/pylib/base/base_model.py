#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import abstractmethod
from pylib.data.shared_cache import cache

class BaseModel(object):
    def __init__(self):
        super(BaseModel, self).__init__()
        # Register model on global cache memory
        # One model on one app
        cache.set(self.__class__.__name__, self)
