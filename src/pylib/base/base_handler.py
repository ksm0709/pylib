#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import abstractmethod
from pylib.data.shared_cache import cache
from cached_singleton import CachedSingleton

class BaseHandler(CachedSingleton):
    def __init__(self, *args, **kwargs):
        pass


if __name__ == '__main__':

    class ChildHandler(BaseHandler):

        def _init_once(self, var):
            self.var = var

        def test(self):
            print('TEST res : {:}'.format(var))


    child = ChildHandler(var=True)
    child_cached = cache.get('ChildHandler')

    child_cached.test()
    assert( child == child_cached )

    print('sucess')

