#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Singleton(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
            cls._instance._init_once(*args, **kwargs)

        return cls._instance

    def __init__(self, *args, **kwargs):
        pass

    def _init_once(self, *args, **kwargs):
        pass


if __name__ == '__main__':
    
    class TestClass(Singleton):
        def _init_once(self, var=True):
            print('init!')
            self.test_var = var
        def get(self):
            return self.test_var

    tc = TestClass(var=True)
    tc2 = TestClass(var=False)

    res = tc2.get()

    print(tc)
    print(tc2)

    assert( res == True )
