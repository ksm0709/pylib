#!/usr/bin/env python
# -*- coding: utf-8 -*-

from singleton import Singleton
from pylib.data.shared_cache import cache

class CachedSingleton(Singleton):
    
    def __new__(cls, *args, **kwargs):
        instance = super(CachedSingleton, cls).__new__(cls, *args, **kwargs)

        if cache.get(instance.__class__.__name__) is None:
            cache.set(instance.__class__.__name__, instance)

        return instance

    def _init_once(self, *args, **kwargs):
        pass


if __name__ == '__main__':
    
    class TestClass(CachedSingleton):
        def _init_once(self, var=True):
            print('init: {:}'.format(var))

        def test(self):
            return cache.get('TestClass')

    tc = TestClass()

    print( tc.test() )
    print( tc )

    assert( tc == tc.test() )
