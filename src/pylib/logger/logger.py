#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pylib.logger.get_logger import get_logger
from pylib.system.timer import Timer
from time import time, sleep
from datetime import date

class cstyle:
    CEND      = '  \33[0m'
    CBOLD     = '\33[1m'
    CITALIC   = '\33[3m'
    CUNDERLINE= '\33[4m'
    CBLINK    = '\33[5m'
    CBLINK2   = '\33[6m'
    CSELECTED = '\33[7m'

    CBLACK  = '\33[30m'
    CRED    = '\33[31m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CBLUE   = '\33[34m'
    CVIOLET = '\33[35m'
    CBEIGE  = '\33[36m'
    CWHITE  = '\33[37m'

    CBLACKBG  = '\33[40m'
    CREDBG    = '\33[41m'
    CGREENBG  = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG   = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG  = '\33[46m'
    CWHITEBG  = '\33[47m'

    CGREY    = '\33[90m'
    CRED2    = '\33[91m'
    CGREEN2  = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2   = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2  = '\33[96m'
    CWHITE2  = '\33[97m'

    CGREYBG    = '\33[100m'
    CREDBG2    = '\33[101m'
    CGREENBG2  = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2   = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2  = '\33[106m'
    CWHITEBG2  = '\33[107m'

class Logger(cstyle):

    __logger = None
    __name = None
    __prev_date = None
    __timer = None

    def __init__(self):
        super(Logger, self).__init__()

    @classmethod
    def status(cls):
        return cls.__logger is not None

    @classmethod
    def set_logger(cls, name=None):
        if name == None:
            import os
            name = os.path.basename(__file__).split('.')[0]

        cls.__logger = get_logger(name, reset=False)

        cls.__name = name
        cls.__prev_date = date.today().strftime('%d')

        if cls.__timer != None:
            cls.__timer.stop()
            cls.__timer = None
    
        cls.__timer = Timer(cls.timer_cb, 1.0)
        cls.__timer.start()

    @classmethod
    def timer_cb(cls):
        from datetime import date
        sample_date = date.today().strftime('%d')

        if sample_date != cls.__prev_date:
            cls.loginfo('Date changed ({:} -> {:}) ... Auto-reloading logfile'.format(cls.__prev_date, sample_date))

            cls.__prev_date = sample_date
            cls.reload()

            cls.loginfo('Auto-reloaded! logging start.')

    @classmethod
    def reload(cls):
        Logger.__logger = get_logger(cls.__name, reset=True)

    @classmethod
    def loginfo(cls, msg, style=''):
        return cls.__logger.info(style + str(msg) + cls.get_cend(style))

    @classmethod
    def logdebug(cls, msg, style=''):
        return cls.__logger.debug(style + str(msg) + cls.get_cend(style))

    @classmethod
    def logwarn(cls, msg, style=''):
        return cls.__logger.warn(style + str(msg) + cls.get_cend(style))

    @classmethod
    def logerror(cls, msg, style=''):
        return cls.__logger.error(style + str(msg) + cls.get_cend(style))

    @classmethod
    def logerr(cls, msg, style=''):
        return cls.__logger.error(style + str(msg) + cls.get_cend(style))

    @classmethod
    def get_cend(cls, style):
        return '' if style == '' else cstyle.CEND

Logger.set_logger()

if __name__ == '__main__':
    Logger.set_logger()

    Logger.loginfo('test info')
    Logger.logwarn('test warn')
    Logger.logdebug('test debug')
    Logger.logerror('test error')
    
