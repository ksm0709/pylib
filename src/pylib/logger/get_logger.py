#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pylib.data.shared_cache import cache
from os.path import isfile, isdir, expanduser
from os import mkdir, listdir, remove, rmdir, symlink, unlink
from shutil import rmtree
import logging
from datetime import date

home_dir = expanduser('~')
logger_dir = home_dir+'/.ros/log/navigation/'

def remove_old_logs():
    global logger_dir

    for f in listdir(logger_dir):
        fdir = logger_dir + f

        if not isdir(fdir):
            remove(fdir)
            continue

        sp = str(f).split('-')

        if len(sp) != 3:
            rmtree(fdir)
            continue
        
        logday = date(int(sp[0]),int(sp[1]),int(sp[2]))
        today = date.today()

        delta = today - logday

        if delta.days > 31:
            rmtree(fdir)

def make_symlink(source, target, repeat=True):
    try:
        symlink(source, target)

    except Exception as e:

        if repeat:
            remove_symlink(target)
            make_symlink(source, target, repeat=False)
        else:
            print(e)

def remove_symlink(target):
    try:
        if(os.path.isdir(target)):
            rmtree(target)
        else:
            unlink(target)
    except Exception as e:
        print(e)


def get_logger(name,reset=False):
    global logger_dir

    # name = log file name
    logger = logging.getLogger(name)

    if reset:
        logger.handlers = []
        remove_old_logs()

    if len(logger.handlers) > 0:
        return logger

    logger_level = cache.get('logger_level', logging.INFO)
    logger.setLevel(logger_level)

    logger_format = cache.get('logger_format', '[%(asctime)s]({:}/%(levelname)-s) %(message)s'.format(name))
    formatter = logging.Formatter(logger_format) 
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    logger_path = logger_dir+'/{:}'.format(date.today().strftime('%Y-%m-%d'))
    logger_file_path = logger_path + '/{:}.log'.format(name)
    logger_link_path = home_dir+'/log'

    if not isdir(logger_dir):
        mkdir(logger_dir)

    if not isdir(logger_path):
        mkdir(logger_path)
        make_symlink(logger_path, logger_link_path)

    file_handler = logging.FileHandler(logger_file_path)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

