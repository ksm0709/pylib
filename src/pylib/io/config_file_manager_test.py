#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase, main
import sys

from config_file_manager import ConfigFileManager
from os.path import expanduser


class ConfigFileManagerTest(TestCase):
    def __init__(self, *args):
        super(ConfigFileManagerTest, self).__init__(*args)

    def setUp(self):
        self.cm = ConfigFileManager(test=True)

    def test_read_info(self):
        config_raw = self.cm.read_info(self.cm.config_info_dir)
        self.assertTrue(len(config_raw['version']) > 0)

    def test_split_name_dir(self):
        filedir, filename = self.cm.split_name_dir('parent/child/file.yaml')

        self.assertTrue(filedir=='parent/child')
        self.assertTrue(filename=='file.yaml')

    def test_get_config_list(self):
        config_list = self.cm.get_config_list(self.cm.config_base_dir)
        for cfg in config_list:
            print('\n\n********* {:}/{:} **********\ntype:{:}\n'.format(cfg.directory, cfg.filename, cfg.filetype))
            print(cfg.data)
        self.assertTrue(len(config_list)>0)

    def test_find_config(self):
        self.cm.initialize()
        cfg = self.cm.find_config(directory='test', filename='test.json')
        self.assertTrue( cfg.data['default']['data'] == 'success' )

    def test_get_value(self):
        self.cm.initialize()
        value = self.cm.get_value('test','test.json', 'default/data')
        self.assertTrue( value == 'success' )

    def test_set_value(self):
        self.cm.initialize()
        res = self.cm.set_value('test','test.json', 'default/data', 'success')
        value = self.cm.get_value('test','test.json', 'default/data')
        self.assertTrue( value == 'success' )

    def test_get_data(self):
        self.cm.initialize()
        data = self.cm.get_data('test','test.ini')
        self.assertTrue( data['default']['data'] == 'success' )

    def test_set_data(self):
        self.cm.initialize()
        data = self.cm.get_data('test','test.ini')
        data['default']['count'] = int(data['default']['count']) + 1

        self.cm.set_data('test','test.ini',data)
        data2 = self.cm.get_data('test','test.ini')

        self.assertTrue( data2['default']['count'] == data['default']['count'] )

    def test_get_value_by_code(self):
        self.cm.initialize()
        value = self.cm.get_value_by_code(cfg_code=101)
        print(value)

    def test_set_value_by_code(self):
        self.cm.initialize()
        res = self.cm.set_value_by_code(cfg_code=101, value=0.7)
        value = self.cm.get_value_by_code(cfg_code=101)
        self.assertTrue( res )
        self.assertTrue( value == 0.7 )


if __name__ == "__main__":
    main()


