#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib

def make_hash_id(text):
    enc = hashlib.md5()
    enc.update(text)
    return enc.hexdigest()

