#!/usr/bin/env python
# -*- coding: utf-8 -*-

from config_file import ConfigFile
from hasher import make_hash_id

from os.path import isfile, expanduser
from os import listdir

import yaml
import os

class ConfigFileManager(object):
    def __init__(self, test=False):
        super(ConfigFileManager, self).__init__()
        self.config_base_dir = expanduser('~')+'/.ros/config'
        self.config_info_dir = self.config_base_dir + '/info.yaml'

        if not test:
            self.initialize()

    def initialize(self):
        config_raw = self.read_info(self.config_info_dir)

        dyanmic_cfg_raw = config_raw.get('dynamic_cfg_info')
        dynamic_cfg_table = {}

        for item in dyanmic_cfg_raw:
            dynamic_cfg_table[str(item.get('cfg_code'))] = item

        config_list = self.get_config_list(self.config_base_dir)
        config_table = {}

        for item in config_list:
            config_table[ item.hash_id ] = item

        self.configs = {"version": config_raw.get('version'), 
                        "message": config_raw.get('message'),
                        "dynamic_cfg": dynamic_cfg_table,
                        "table": config_table}

    def read_info(self, filename):
        with open(filename, 'r') as yaml_raw:
            return yaml.load(yaml_raw)


    def split_name_dir(self, directory):
        sp_dir = directory.split('/')
        filename = sp_dir[-1]
        filedir = '/'.join(sp_dir[:-1])
        return filedir, filename

    def get_config_list(self, directory):

        if isfile(directory):

            filedir, filename = self.split_name_dir(directory)
            cfg = ConfigFile(directory=filedir, filename=filename)
            return [cfg]

        cfg_list = []
        for child in listdir(directory):
            if child[0] == '.':
                continue

            cfg_list += self.get_config_list(directory+'/'+child)

        return cfg_list

    def get_data(self, directory, filename):
        cfg = self.find_config(directory, filename)

        if cfg == None :
            raise Exception('failed to read config file [{:}]'.format(directory+'/'+filename))

        return cfg.get_data()

    def set_data(self, directory, filename, data):
        cfg = self.find_config(directory, filename)

        if cfg == None :
            raise Exception('failed to read config file [{:}]'.format(directory+'/'+filename))

        if not cfg.set_data(data):
            raise Exception('failed to set data : invalid data')

    def get_value_by_code(self, cfg_code):
        cfg_info = self.configs['dynamic_cfg'].get(str(cfg_code))

        directory = cfg_info.get('directory')
        filename = cfg_info.get('filename')
        ns = cfg_info.get('datans')

        return self.get_value(directory=directory, filename=filename, ns=ns)

    def set_value_by_code(self, cfg_code, value):
        cfg_info = self.configs['dynamic_cfg'].get(str(cfg_code))

        directory = cfg_info.get('directory')
        filename = cfg_info.get('filename')
        ns = cfg_info.get('datans')

        return self.set_value(directory=directory, filename=filename, ns=ns, value=value)

    def get_value(self, directory, filename, ns):
        cfg = self.find_config(directory, filename)

        if cfg == None :
            raise Exception('failed to read config file [{:}]'.format(directory+'/'+filename))

        return cfg.get_value(ns)

    def set_value(self, directory, filename, ns, value):
        cfg = self.find_config(directory, filename)

        if cfg == None :
            raise Exception('failed to read config file [{:}]'.format(directory+'/'+filename))

        return cfg.set_value(ns, value)

    def find_config(self, directory, filename):
        full_path = self.config_base_dir
        
        if directory != '' : 
            full_path = full_path + '/' + directory

        full_path = full_path + '/' +filename

        hash_id = make_hash_id(full_path)
        return self.configs['table'].get(hash_id)

    def get_version_info(self):
        return {'version': self.configs.get('version'), 'message': self.configs.get('message'), 'stamp': 'none'}

    def set_version_info(self, version, message):
        try:
            self.configs['version'] = version
            self.configs['message'] = message

            self.set_value( directory='', filename='info.yaml', ns='version', value=version )
            self.set_value( directory='', filename='info.yaml', ns='message', value=message )

        except:
            return False
        return True

        

