#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase, main
import sys

from os.path import expanduser
from config_file import ConfigFile, make_hash_id

class ConfigFileTest(TestCase):
    def __init__(self, *args):
        super(ConfigFileTest, self).__init__(*args)

    def setUp(self):
        self.config_base_dir = expanduser('~')+'/.ros/config'

    def get_dir(self, subdir=''):
        return self.config_base_dir + '/' + subdir


    def test_get_filetype(self):
        cfg_ini = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        cfg_yaml = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        cfg_json = ConfigFile(filename="test.json", directory=self.get_dir("test"))

        self.assertEqual(cfg_ini.get_filetype(), "ini")
        self.assertEqual(cfg_yaml.get_filetype(), "yaml")
        self.assertEqual(cfg_json.get_filetype(), "json")

    def test_read_ini(self):
        cfg = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        data = cfg.read_ini()

        self.assertEqual(cfg.is_valid(), True)
        self.assertEqual(data['default']['data'], "success")

    def test_read_json(self):
        cfg = ConfigFile(filename="test.json", directory=self.get_dir("test"))
        data = cfg.read_json()

        self.assertEqual(cfg.is_valid(), True)
        self.assertEqual(data['default']['data'], u"success")

    def test_read_yaml(self):
        cfg = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        data = cfg.read_yaml()

        self.assertEqual(cfg.is_valid(), True)
        self.assertEqual(data['default']['data'], "success")

    def test_nested_set(self):
        cfg = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        d = {'A':{'B':1}}
        cfg.nested_set(d, ['A','B'], 2)
        self.assertTrue(d['A']['B'] == 2)

    def test_make_hash_id(self):
        cfg = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        config_dir = expanduser('~')+'/.ros/config/test/test.yaml'
        hash_id = make_hash_id(config_dir)
        self.assertTrue( cfg.hash_id == hash_id )

    def test_save_json(self):
        cfg = ConfigFile(filename="test.json", directory=self.get_dir("test"))
        value = int(cfg.get_value('default/count'))
        cfg.set_value('default/count', value+1)
        cfg.save_json()

        cfg2 = ConfigFile(filename="test.json", directory=self.get_dir("test"))
        value2 = int(cfg2.get_value('default/count'))
        self.assertTrue( value2 == value+1 )

    def test_save_yaml(self):
        cfg = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        value = int(cfg.get_value('default/count'))
        cfg.set_value('default/count', value+1)
        cfg.save_yaml()

        cfg2 = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        value2 = int(cfg2.get_value('default/count'))
        self.assertTrue( value2 == value+1 )

    def test_save_ini(self):
        cfg = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        value = int(cfg.get_value('default/count'))
        cfg.set_value('default/count', value+1)
        cfg.save_ini()

        cfg2 = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        value2 = int(cfg2.get_value('default/count'))
        self.assertTrue( value2 == value+1 )

    def test_get_data(self):
        cfg_ini = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        cfg_yaml = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        cfg_json = ConfigFile(filename="test.json", directory=self.get_dir("test"))

        data_ini = cfg_ini.get_data()
        data_yaml = cfg_yaml.get_data()
        data_json = cfg_json.get_data()

        print(data_ini)
        print(data_yaml)
        print(data_json)

    def test_set_data(self):
        cfg_ini = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        cfg_yaml = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        cfg_json = ConfigFile(filename="test.json", directory=self.get_dir("test"))

        res_ini = cfg_ini.set_data({"default":{"data":"success", "count":0}})
        res_yaml = cfg_yaml.set_data({"default":{"data":"success", "count":0}})
        res_json = cfg_json.set_data({"default":{"data":"success", "count":0}})

        self.assertTrue( res_ini )
        self.assertTrue( res_yaml )
        self.assertTrue( res_json )

        print(cfg_ini.data)
        print(cfg_yaml.data)
        print(cfg_json.data)

    def test_final(self):
        cfg_ini = ConfigFile(filename="test.ini", directory=self.get_dir("test"))
        cfg_yaml = ConfigFile(filename="test.yaml", directory=self.get_dir("test"))
        cfg_json = ConfigFile(filename="test.json", directory=self.get_dir("test"))

        self.assertEqual(cfg_ini.data['default']['data'], "success")
        self.assertEqual(cfg_json.data['default']['data'], "success")
        self.assertEqual(cfg_yaml.data['default']['data'], "success")
        

if __name__ == "__main__":
    main()


