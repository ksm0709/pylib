#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml, json
import ConfigParser as configparser
from hasher import make_hash_id

import os
from collections import OrderedDict

class ConfigFile(object):

    def __init__(self, directory, filename, filetype=''):
        super(ConfigFile, self).__init__()
        self.filename = str(filename)
        self.filetype = str(filetype)
        self.directory = str(directory)
        self.data = {"error":0}

        if filetype == '' :
            self.filetype = self.get_filetype()

        self.file_full_path = self.directory +'/'+self.filename
        self.hash_id = make_hash_id(self.file_full_path)

        if not self.check_file_exist():
            return

        self.read_from_file()

    def get_filetype(self):
        return self.filename.split('.')[1]

    def is_valid(self):
        return self.data.get("error") == None

    def check_file_exist(self):
        return os.path.exists(self.file_full_path)

    def read_ini(self):
        parser = configparser.ConfigParser()
        parser.read(self.file_full_path)

        return parser._sections

    def read_json(self):
        with open(self.file_full_path, 'r') as json_raw:
            return json.load(json_raw)

    def read_yaml(self):
        with open(self.file_full_path, 'r') as yaml_raw:
            return yaml.load(yaml_raw)

    def save_json(self):
        with open(self.file_full_path, 'w') as json_file:
            json.dump(self.data, json_file)

    def save_yaml(self):
        with open(self.file_full_path, 'w') as yaml_file:
            yaml.dump(self.data, yaml_file)

    def save_ini(self):
        parser = configparser.ConfigParser()
        parser._sections = self.data

        with open(self.file_full_path, 'w') as ini_file:
            parser.write(ini_file)

    def set_value(self, ns, value):
        res = self.nested_set(self.data, ns.split('/'), value)
        self.save_to_file()
        return res

    def get_value(self, ns):
        keys = ns.split('/')
        data = self.data
        for key in keys:
            data = data.get(key)
        return data

    def nested_set(self, dic, keys, value):
        d = dic
        for key in keys[:-1]:
            if key in d:
                d = d[key]
        if keys[-1] in d:
            d[keys[-1]] = value
            return True
        return False   
    
    def save_to_file(self):
        if self.filetype == "ini":
            self.save_ini()
        elif self.filetype == "json":
            self.save_json()
        elif self.filetype == "yaml":
            self.save_yaml()
       
    def read_from_file(self):
        if self.filetype == "ini":
            self.data = self.read_ini()
        elif self.filetype == "json":
            self.data = self.read_json()
        elif self.filetype == "yaml":
            self.data = self.read_yaml()
        self.read = True

    def get_data(self):
        if self.filetype=='ini':
            data_dict = {s:dict(self.data[s]) for s in self.data.keys()}
            return data_dict

        return self.data

    def set_data(self, data):
        if not isinstance(data, dict):
            return False

        if self.filetype=='ini':
            self.data = OrderedDict({s:OrderedDict(data[s]) for s in data.keys()})
            self.save_ini()
            return True

        self.data = data
        self.save_to_file()
        return True

            








