#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
from threading import Lock
from pylib.data import cache

def func_lock(func):

    @wraps(func)
    def wrapper(*args, **kwargs):

        locker_name = func.__name__+'/lock'
        locker = cache.get(locker_name)

        if locker is None:
            locker = Lock()
            cache.set(locker_name, locker)

        with locker:
            return func(*args, **kwargs)

    return wrapper

    
def func_locked(func):
    locker_name = func.__name__+'/lock'
    locker = cache.get(locker_name)
    if locker is None:
        return False
    return locker.locked()

