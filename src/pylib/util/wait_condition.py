#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

def wait_condition(checker, timeout=10.0, interval=0.01):
    start_time = time.time()

    time_checker = lambda: True

    if timeout > 0 :
        time_checker = lambda: (time.time() - start_time) < timeout

    while time_checker() and not checker() :
        time.sleep(interval)

    return checker()

