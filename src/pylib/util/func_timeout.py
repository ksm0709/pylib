#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pylib.system import Timer
from functools import wraps
import signal

def func_timeout(timeout=10, timeout_log='', function=None):

    def decorator(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            if timeout == 0 :
                return func(*args, **kwargs)

            runner = Timer(func, 0, oneshot=True, args=args, kwargs=kwargs)
            timer = Timer(lambda: None, timeout, oneshot=True)

            timer.start()
            runner.start()

            while timer.is_running():
                if not runner.is_running():
                    timer.trigger_stop()
                    return runner.result
                    
            runner.trigger_stop()
            raise Exception('Function [{}] timeout'.format(func.__name__))

        return wrapper

    if function == None:
        return decorator

    else:
        return decorator(function)


if __name__ == '__main__':
    import time

    @func_timeout(2.5)
    def test_loop(arg):
        start_time = time.time()
        print('start loop with arg : {}'.format(arg))

        while True:
            dur = time.time() - start_time
            print('loop run ({:.1f})'.format(dur))

            if dur > arg:
                return True

            time.sleep(0.1)

    try:
        res = test_loop(100)
        print(res)
    except Exception as e:
        print('\nException : {}\n'.format(e))

    time.sleep(1.0)
    
    try:
        res = test_loop(2)
        print(res)
    except Exception as e:
        print('\nException : {}\n'.format(e))
       

