from pylib.data.locked_data import LockedData

class SharedCache(object):
    __cache = {}

    @classmethod
    def get(cls, name, default=None):
        res = cls.__cache.get(name)
        return res if res!=None else default

    @classmethod
    def set(cls, name, value):
        cls.__cache[name] = value

    @classmethod
    def get_locked(cls, name, default=None):
        res = cls.__cache.get(name).data
        return res if res!=None else default

    @classmethod
    def set_locked(cls, name, value):
        if cls.__cache.get(name) == None:
            cls.__cache[name] = LockedData()
            
        cls.__cache[name].data = value

    @classmethod
    def __repr__(cls):
        return cls.__cache

    def __str__(self):
        return SharedCache.__cache.__str__()

cache = SharedCache()
