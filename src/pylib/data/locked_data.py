from threading import Lock

class LockedData(object):
    '''
    init_value == None : Unspecified data type. Any data type can be set.
    init_value != None : Specified data type. Data has same type with given value can be set.
    '''
    def __init__(self, init_value=None):
        self.__lock = Lock()
        self.__data = init_value
        self.__type_checker = lambda data, value: None

        if init_value is not None:

            def type_checker(data, value):
                if type(data) != type(value):
                    raise TypeError('Invalid type : trying to set {:} type data to {:} type LockedData'.format(type(value), type(data)))

            self.__type_checker = type_checker 

    def get(self):
        with self.__lock :
            return self.__data

    def set(self, value):
        with self.__lock :
            self.__type_checker(self.__data, value)
            self.__data = value

    @property
    def data(self):
        with self.__lock :
            return self.__data

    @data.setter
    def data(self, value):
        with self.__lock :
            self.__type_checker(self.__data, value)
            self.__data = value


