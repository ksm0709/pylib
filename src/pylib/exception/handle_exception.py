#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
from functools import wraps
from pylib.exception import CodedException
from pylib.logger import Logger
from pylib.data import cache
import logging

def handle_exception(handler=lambda *hargs, **hkwargs: None, trace=True, func=None):

    def trace_log(msg):
        try:
            Logger.logerror(msg)
        except:
            print(msg)

    tracer = lambda: trace_log(traceback.format_exc()) if trace else lambda: None

    def decorator(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)

            except Exception as ex:
                tracer()
                #Logger.logerror(traceback.format_exc())

                # execute handler(eg. publish to ros/write to file... etc) 
                try:
                    return handler(*args, **dict(kwargs, ex=ex))
                except:
                    return handler(ex) #old

            return result

        return wrapper

    if func is None:
        return decorator
    else:
        return decorator(func)

if __name__ == '__main__':

    @handle_exception(lambda var, ex: var)
    def test_func(var):
        raise Exception('test')

    print(test_func(111))

    Logger.set_logger('test')

    print(test_func(222))

