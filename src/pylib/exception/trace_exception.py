#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
from pylib.logger import Logger
from functools import wraps

def trace_exception(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)

        except Exception as ex:
            Logger.logerror(traceback.format_exc())
            raise 

    return wrapper

if __name__ == '__main__':

    Logger.set_logger()

    @trace_exception
    def test_func():
        raise Exception('test')

    test_func()

