#!/usr/bin/env python
# -*- coding: utf-8 -*-

class CodedException(Exception):
    def __init__(self, code, msg):
        self.__code = code
        self.__msg = msg

    def __str__(self):
        return self.__msg

    @property
    def code(self):
        return self.__code

    @property
    def msg(self):
        return self.__msg
        
        
