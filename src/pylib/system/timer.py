#!/usr/bin/env python
from pylib.data import LockedData
import threading
import time
import ctypes
import atexit

class Timer(object):
    def __init__(self, callback, interval=0.1, oneshot=False, args=[], kwargs={}, stopper=lambda: False):
        super(Timer, self).__init__()
        self.__thread = None
        self.__cb = callback
        self.__interval = interval
        self.__args = args
        self.__kwargs = kwargs
        self.__result = LockedData()
        self.__runner = self.select_runner(oneshot)
        self.__stopper = stopper
        self.__running = False

    def run(self):
        self.__runner()
        self.__running = False

    def start(self):
        if self.__running :
            self.stop()

        self.__thread = threading.Thread(target=self.run)
        self.__thread.daemon = True
        self.__running = True

        self.__thread.start()

    def set_args(self, args=[]):
        self.__args = args

    def set_kwargs(self, kwargs={}):
        self.__kwargs = kwargs

    def is_running(self):
        return self.__running

    def select_runner(self, oneshot):
        return self.oneshot_runner if oneshot else self.loop_runner

    def oneshot_runner(self):
        self.sleep(self.__interval)

        if not self.__running:
            return

        self.__result.data = self.__cb(*self.__args, **self.__kwargs)

    def loop_runner(self):
        while True:
            self.sleep(self.__interval)

            if self.__stopper():
                self.stop()

            if not self.__running:
                return

            self.__result.data = self.__cb(*self.__args, **self.__kwargs)

    def sleep(self, duration=1.0):
        try:
            start_time = time.time()
            while time.time() - start_time < duration :
                time.sleep(0.01)
        except:
            return

    def stop(self):
        if self.__thread is None:
            return 
        
        if self.__thread.isAlive():
            self.trigger_stop()
            self.join()
            self.__thread = None

    def trigger_stop(self):
        self.__running = False
        self.raise_exception()

    def join(self):
        while self.__thread.isAlive():
            time.sleep(0.01)
        #print('Timer Stopped')

    def raise_exception(self):
        thread_id = ctypes.c_long(self.__thread.ident)
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, ctypes.py_object(SystemExit))
        if res != 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)


    @property
    def result(self):
        return self.__result.data

if __name__ == '__main__':
    print('Test start')

    class Test:
        def __init__(self):
            pass

        def start(self):
            self.timer = None
            self.timer = Timer(self.callback, 5.0, oneshot=True, args=['success'])
            self.timer.start()
            print('Timer Start')

        def callback(self, arg):
            return True, 'callback called with argument : {:}'.format(arg)

        def stop(self):
            print('Stopping')
            self.timer.stop()
            print('Stopped')

    test = Test()
    test.start()

    time.sleep(2.0)
    test.stop()

    time.sleep(1.0)
    test.start()
    
    while True:
        print(test.timer.result)
        if test.timer.result is not None:
            break
        time.sleep(1.0)

    print('Test success')
