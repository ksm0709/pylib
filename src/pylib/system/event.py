from pymitter import EventEmitter
from functools import wraps
from time import sleep, time
from timer import Timer
from pylib.data.locked_data import LockedData

class EventModel(object):
    __emitter = None 
    __delimiter = None

    def __init__(self, wildcard=False, delimiter='/'):
        if EventModel.__emitter == None:
            EventModel.__emitter = EventEmitter(wildcard=wildcard, delimiter=delimiter)
            EventModel.__delimiter = delimiter

    def on(self, event, response=False):

        def deco(func,*args,**kwargs): 
            @wraps(func)
            def wrapper(*args, **kwargs):
                if response:
                    result = func(*args, **kwargs)

                    EventModel.__emitter.emit( self.response_ns(event), {'event':event, 'from': func.__name__,'result':result})

                    return result

                timer = Timer(func, 0.0, oneshot=True, args=args, kwargs=kwargs)
                timer.start()

            return wrapper

        def wrap_on(func, *args, **kwargs):
            if response and len(EventModel.__emitter.listeners(event)) > 0:
                raise Exception('Event with option [response=True] can have one callback only')

            func_deco = deco(func)
            EventModel.__emitter.on(event, func_deco)
            return func_deco
        return wrap_on

    def off(self, *args, **kwargs):
        EventModel.__emitter.off(*args, **kwargs)

    def response_ns(self, event):
        return event + EventModel.__delimiter + 'response'

    def remit(self, event, timeout, *args, **kwargs):
        _buf = LockedData()
        _flag = LockedData(False)
        def callback(response):
            _buf.data = response
            _flag.data = True

        response_event = self.response_ns(event)
        EventModel.__emitter.on(response_event, callback)
        EventModel.__emitter.emit(event, *args, **kwargs)

        retval = None
        if self.wait_condition( lambda: _flag.data , timeout=timeout) :
            retval = _buf.data.get('result')

        EventModel.__emitter.off(response_event, callback)

        return retval

    def emit(self, event, *args, **kwargs):
        EventModel.__emitter.emit(event, *args, **kwargs)

    def wait_condition(self, cond, timeout):
        stime = time()
        while time()-stime < timeout and not cond() :
            sleep(0.1)
        return cond()

### global event variable ####
event = EventModel()
##############################

if __name__ == "__main__":

    @event.on('test_event', response=True)
    def test_cb(arg):
        print('CB1 start: {:}'.format(arg))
        sleep(2.0)
        print('CB1 end')

        return True
    
    @event.on('test_event2', response=False)
    def test_cb2(arg):
        print('CB2 start : {:}'.format(arg))
        sleep(1.0)
        print('CB2 running ..')
        sleep(1.0)
        print('CB2 end')

    @event.on('test_event/response')
    def test_response(arg):
        print(' --> response : {:}'.format(arg))

    print('---------------------------------------------')
    print('* test_event remit : async mode \n')

    res = event.remit('test_event', 3.0, {'data':'hello world'})
    assert(res == True)

    sleep(1.0)
    print('---------------------------------------------')
    print('* test_event2 emit : sync mode \n')

    res = event.emit('test_event2', {'data':'hello world'})
    assert(res == None)

    print('---------------------------------------------')
    print('* waiting sync function \n')

    sleep(4.0)

