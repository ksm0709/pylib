#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, rosgraph
from pylib.system import Timer
from pylib.util import func_timeout
from pylib.logger import Logger
from pylib.exception import handle_exception

from threading import Lock

class MockServiceProxy(object):
    def call(self, req):
        return None

class ServiceClientProxy(object):
    def __init__(self, srvname, srvtype, mock=MockServiceProxy(), test=False):
        super(ServiceClientProxy, self).__init__()
        self.__srv = mock
        self.__srvtype = srvtype
        self.__srvname = srvname

        if not test:
            self.__srv = rospy.ServiceProxy(srvname,srvtype)

    def call(self, req=None, timeout=0.0):
        if req == None:
            req = self.__srvtype._request_class()

        def exception_handler(ex):
            Logger.logwarn('Service call [{:}] failed : {:}'.format(self.__srvname, ex))
            return self.__srvtype._response_class()

        @handle_exception(handler = exception_handler)
        @func_timeout(timeout=timeout, timeout_log=self.__srvname)
        def service_call(caller, req):
            return caller.call(req)
            
        return service_call(self.__srv, req)

if __name__ == '__main__':

    from std_srvs.srv import Trigger, TriggerRequest

    Logger.set_logger('root')

    scproxy = ServiceClientProxy('/nav/map_stop', Trigger)

    Logger.loginfo('Call service')

    res = scproxy.call(TriggerRequest(), timeout=5.0)

    Logger.loginfo(res)
