#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pylib.logger import Logger
import rospy

class MockImpl(object):
    def set_queue_size(self, s):
        pass

class MockPublisher(object):
    def __init__(self):
        self.impl = MockImpl()

    def publish(self, msg):
        print('test pub: {:}'.msg)

class PublisherProxy(object):
    __pubs = {}

    def __init__(self, topic, msgtype, queue_size=1, test=False):
        cls = type(self)
        self._topic = topic

        if cls.__pubs.get(topic) is None :
            cls.__pubs[topic] =  MockPublisher() if test else rospy.Publisher(topic, msgtype, queue_size=queue_size)

        else:
            cls.__pubs[topic].impl.set_queue_size(queue_size)

    def publish(self, *args, **kwargs):
        PublisherProxy.__pubs[self._topic].publish(*args, **kwargs)

    @property
    def publisher(self):
        return PublisherProxy.__pubs[self._topic]


if __name__ == '__main__':
    
    print('test publisher proxy')

    from std_msgs.msg import Empty

    pub1 = PublisherProxy(test=True, topic='/test_topic', queue_size=1, msgtype=Empty)
    pub2 = PublisherProxy(test=True, topic='/test_topic', queue_size=1, msgtype=Empty)
    pub3 = PublisherProxy(test=True, topic='/test_topic', queue_size=2, msgtype=Empty)
    pub4 = PublisherProxy(test=True, topic='/test_topic_', queue_size=1, msgtype=Empty)

    print(pub1.publisher)
    print(pub2.publisher)
    print(pub3.publisher)
    print(pub4.publisher)

    assert( pub1.publisher == pub2.publisher )
    assert( pub1.publisher == pub3.publisher )
    assert( pub1.publisher != pub4.publisher )
    assert( pub3.publisher != pub4.publisher )

    print('success')


