#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, rosgraph
from pylib.system import Timer
from pylib.util import func_timeout
from pylib.logger import Logger

from threading import Lock

class ServiceServerProxy(object):
    def __init__(self, srvname, srvtype, callback=lambda req: None, timeout=0, test=False):
        super(ServiceServerProxy, self).__init__()
        self.__srv = None
        self.__user_callback = func_timeout(timeout=timeout, function=callback)
        self.__lock = Lock()

        if not test :
            self.__srv = rospy.Service(srvname, srvtype, self.execute)

    def execute(self, req):

        with self.__lock :
            try:
                return self.__user_callback(req)
            except Exception as e:
                Logger.logwarn(e)

if __name__ == '__main__':

    from std_srvs.srv import Trigger

    Logger.set_logger('root')

    def callback(req):
        print(req)

        import time
        while True:
            time.sleep(0.1)
            

    ssproxy = ServiceServerProxy('any_service', Trigger, callback, timeout=3)

    ssproxy.execute('start')
