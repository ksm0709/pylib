from pylib.logger import Logger
from pylib.exception import handle_exception
import rospy

class RosParamProxy(object):

    @staticmethod
    @handle_exception(lambda ns, default, ex: default)
    def get(ns, default=None):
        return rospy.get_param(ns, default)

    @staticmethod
    @handle_exception(lambda ex: False)
    def set(ns, value):
        rospy.set_param(ns, value)
        return True

    @staticmethod
    def set_param(ns, value):
        return RosParamProxy.set(ns, value)

    @staticmethod
    def get_param(ns, default=None):
        return RosParamProxy.get(ns, default)

param = RosParamProxy()

if __name__ == "__main__":

    param.set('/test',2)
    value = param.get('/test',1)
    print(value)
