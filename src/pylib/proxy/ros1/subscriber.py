import rospy

from pylib.data import LockedData

class MockImpl(object):
    def set_queue_size(self, s):
        pass

class MockSubscriber(object):
    def __init__(self, name, data_class, callback, queue_size):
        self._name = name
        self._data_class = data_class
        self._callback = callback
        self._queue_size = queue_size
        self.impl = MockImpl()

    def call(self, msg):
        self._callback(msg)
        

class SubscriberProxy(object):
    __subs = {}

    def __init__(self, topic, msgtype, callback=lambda msg: None, queue_size=10, test=False):
        subscriber_object = MockSubscriber if test else rospy.Subscriber
        cls = type(self)

        self._topic = topic

        if cls.__subs.get(topic) is None:

            def _main_callback(msg):
                SubscriberProxy.__subs[topic]['latest_msg'].data = msg
                for cb in SubscriberProxy.__subs[topic]['callbacks']:
                    cb( msg )

            
            cls.__subs[topic] = {'sub': subscriber_object(name=topic,
                                                        data_class=msgtype, 
                                                        callback=_main_callback, 
                                                        queue_size=queue_size),
                                 'callbacks': [callback],
                                 'latest_msg': LockedData()
                                 }

        else:
            cls.__subs[topic]['sub'].impl.set_queue_size(queue_size)
            cls.__subs[topic]['callbacks'].append( callback )

    @property
    def latest_msg(self):
        return SubscriberProxy.__subs[self._topic]['latest_msg'].data

    @property
    def subscriber(self):
        return SubscriberProxy.__subs[self._topic]['sub']

if __name__ == '__main__':
    print('test subscriber proxy')

    from std_msgs.msg import String

    def callback1(msg):
        print('cb1 : ' + msg.data)
    def callback2(msg):
        print('cb2 : ' + msg.data)
    
    sub1 = SubscriberProxy('sub_topic', String, callback1, queue_size=1, test=True)
    sub2 = SubscriberProxy('sub_topic', String, callback2, queue_size=2, test=True)

    subscriber1 = sub1.subscriber
    subscriber2 = sub2.subscriber

    print(subscriber1)
    print(subscriber2)

    assert( subscriber1 != None )
    assert( subscriber1 == subscriber2 )
    
    msg = String('hello world!')
    subscriber1.call( msg=msg )

    assert( sub1.latest_msg == msg )
    assert( sub2.latest_msg == msg )

    print('success')
