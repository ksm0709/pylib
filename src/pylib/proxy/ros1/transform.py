#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pylib.exception import handle_exception
from pylib.logger import Logger
import rospy
import tf

class TransformProxy(object):
    __tf = None 

    def __init__(self):
        super(TransformProxy, self).__init__()

        # One TF listener on One process
        if TransformProxy.__tf is None :
            TransformProxy.__tf = tf.TransformListener()

    @handle_exception(lambda ex: None, trace=False)
    def canTransform(self, target_frame, source_frame, timestamp=rospy.Time(0)):
        return TransformProxy.__tf.canTransform(target_frame, source_frame, timestamp)

    @handle_exception(lambda ex: None, trace=False)
    def lookupTransform(self, target_frame, source_frame, timestamp=rospy.Time(0)):
        return TransformProxy.__tf.lookupTransform(target_frame, source_frame, timestamp)

    @handle_exception(lambda ex: None, trace=False)
    def transformPose(self, target_frame, pose):
        return TransformProxy.__tf.transformPose(target_frame, pose)

    @handle_exception(lambda ex: None, trace=False)
    def waitForTransform(self, target_frame, source_frame, timestamp=rospy.Time(0), waittime=rospy.Duration(1.0)):
        return TransformProxy.__tf.waitForTransform(target_frame, source_frame, timestamp, waittime)





